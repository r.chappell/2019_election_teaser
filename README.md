# 2019_election_teaser

A shortened teaser version of the 2019 Australian Federal Election Dashboard. This will be used on the Griffith website and links to the full dashboard. Teaser: https://regionalinnovationdatalab.shinyapps.io/data_dash/  Built: R/ leaflet/R.Shiny
